# NetFlix Clone with Flutter / Firebase

## Watch Out for running flutter run Error: Null safety features are disabled for this library

```
flutter run --no-sound-null-safety

OR

Add Ignore the Error Message Settings in your Editor
```

## Firebase-firestore

pubspec.yaml
```
cloud_firestore: ^3.4.2
firebase_core: ^1.20.0
```

You must create a model value directly from the Firebase console **like this** 

<img width="80%" src="/uploads/54d20843de0fa488b3499c7f39c31240/스크린샷_2022-09-26_오후_4.37.14.png"/>


If you created a database in test mode, you must update the test mode or switch to production mode before **30 days** have elapsed.

